Version 0.6.1
=============
Released: 2020-01-26

 * Fixed error when OEM specific DMI type is present on the dmidecode output
 * Fixed Hardware monitor values having up to 15 decimals for some users
 * Fixed Core usage LevelBar changing colors due to default offsets

Version 0.6.0
=============
Released: 2020-01-26

 * Name changed to GtkStressTesting

Version 0.5.0
=============
Released: 2020-01-25

 * Initial release
